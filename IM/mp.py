#-------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Alan
#
# Created:     22/12/2012
# Copyright:   (c) Alan 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------
#!/usr/bin/env python

import sys
import os
from . import common
import multiprocessing
from tempfile import TemporaryFile
import pickle
import warnings

PROCESSES = multiprocessing.cpu_count()
DEBUG = False


def calculate(func, args):
    result = func(*args)
    return result


from tempfile import TemporaryFile
import pickle


def multiple_processes(f, iterable, save_new=False, args=()):
    """
    spawn multiple processes.

    f is a function
    iterable is an iterable type and may include Storage instances but these are
    converted to string representations which must be loaded by f
    f may not return objects and so must save any relevant data prior to completion.

    def f(v):
        ''' Work done in a multiple process.
        You cannot pass objects through this function
        '''
        v=IM.VC7(v)
        out = v.new_vc7()
        out.save()
        return str(out)

    NOTE FOR WINDOWS:
        you need to include the following prior to running this function: multiprocessing.freeze_support()

    import multiprocessing
    if __name__ == '__main__':
        multiprocessing.freeze_support()

    """

    #
    # Create pool
    #
    def make_usable(t):
        """ Multiprocess relies of pickles to pass data and does not pass
            Storage type objects.
            Subclassed storage objects are returned as their string representation
            and if save_new is True then any objects that are passed are automatically
            saved for subsequent retrival but requres that the object is called in
            f
        """
        if isinstance(t, common.Storage):
            if save_new:
                if not os.path.isfile(str(t)):
                    if not t.loaded:
                        warnings.warn(
                            'No useful data in object:{0} but saving anyway'.format(t))
                    t.notes = 'Saved as part of mutiple_processes for function:'\
                              ' {0}\ndocstring{1}'.format(
                                  f.__name__, f.__doc__)
                    t.save(quiet=True)
            return str(t)

        try:
            pickle.dump(t, TemporaryFile())
            return t
        except BaseException:
            raise pickle.PickleError(
                'Unable to pickle object hence cannot multiprocess:'.format(t))
            # PUT HANDLERS HERE AS REQUIRED
    if not DEBUG:
        pool = multiprocessing.Pool(PROCESSES)

    try:
        iterable = [make_usable(t) for t in iterable if t]

        args = [make_usable(t) for t in args]
        TASKS = [(f, [t] + list(args)) for t in iterable]

        if DEBUG:
            for t in TASKS:
                yield t[0](*t[1])
        else:
            results = [pool.apply_async(calculate, t) for t in TASKS]
            try:
                for r in results:
                    yield r.get()
            except BaseException:
                index = results.index(r)
                print('Error for iterable {0}: {1}'.format(
                    index, TASKS[index]))
                raise

    finally:
        if 'pool' in dir():
            pool.terminate()
            pool.join()


# demo usage
#-------------------------------------------------------------------------

def demof(v):
    """ Work done in a multiple process.
    You cannot pass objects through this function

    """
    from . import common
    common.LOAD_ATTEMPTS_WRITE = 6

    from . import core
    v = core.VC7(v)

# out.save()
    return repr(v.as_repr())


if __name__ == '__main__':

    from . import core

    PROCESSES = 7
    DEBUG = 0

    multiprocessing.freeze_support()


# if __name__ == '__main__':
# multiprocessing.freeze_support()
# def f(v):
# """ function which can do stuff independently in a new process
# """
# print v
##
    v1 = core.demo_VC7()
    from .common import ReprInst

    from .core import VC7

    for out in multiple_processes(demof, [v1] * 50, save_new=True):
        v = eval(out).get_inst()
