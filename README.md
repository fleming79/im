Overview
========

IM is a Python module designed to work with multi-dimensional images and vectors using numpy arrays. It was developed to load DaVis 8 files 'IM7' and 'VC7' and provides higher level access compared to the PyPi module [ReadIM](https://pypi.org/project/ReadIM/).

Files are loaded via the classes `IM7(filename)` and `VC7(filename)`. Writing back to IM7 and VC7 files is provided by the method `writeIM7` and `writeVC7`.

Note: Experimental support for 3D vector fields.

Requires
--------

The following Python modules are required as part of the installation.

* ReadIM
* matplotlib
* scipy
* pandas
* Pillow

Installation
------------

You can install the latest version directly with pip from Bitbucket. Note that a different package [IM](https://pypi.org/project/IM/) already exists on PyPi, so you can't just install this Module from the PyPi repository.

```pip install git+https://bitbucket.org/fleming79/im```

Alternatively, you can clone the repository and add the root of IM to PYTHONPATH (or `sys.path.insert(0, 'path to IM')`).

Usage
-----

```python
import IM
from pylab import *
v=IM.demo_VC7()
```

Visualise the velocity field

```python
v.browse()
show()      # use left & right keys to navigate
v.show()
show()
v.Vx.shape
v.Vy.shape
v.frames
```

Some numpy functionality is enabled (if rational)

* Slicing frames

```python
v[0:1]
```

* maximum (across all frames)

```python
v.max()
v.max().frames
```

* maximum (for x and y velocity fields)

```python
v.max(None)
```

Read IM7 files
--------------

```python
import ReadIM
im = IM.IM7(ReadIM.extra.get_sample_image_filenames()[0])
im.writeIM7('mynewfile.im7')
```

Read VC7 files
--------------

Reading vectors is just as easy. Note that when writing the folder is specified rather than the name.

```python
import ReadIM
v = IM.VC7(ReadIM.extra.get_sample_vector_filenames()[0])
v.writeVC7('new_vector_folder_name')
```

Sometimes it is convenient to group vectors or images as a sequence of frames.

```python
v2 = IM.filter.create_group('combined', ims=[v,v,v])
v2.writeVC7('new_vector_folder_name')
```

Persistence
-----------

Data can be persisted with pickle or [hickle](https://pypi.org/project/hickle/).

```python
v.save(write_mode='hickle')
```

Serialization
-------------

Direct serialization of the objects is not supported, however it is possible to pass an IObytes object to the save method to achieve a similar outcome.

```python
import io
dst = io.BytesIO()
a = IM.demo_VC7()
a.save(dst, write_mode='pickle')
b = IM.VC7(dst)
```
