import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname( __file__), '..')))
import IM
from IM.common  import Storage, __storage_new__, __storage_init__
#-------------------------------------------------------------------------

from numpy.testing import (TestCase, assert_almost_equal, assert_equal,
                           assert_, assert_raises, run_module_suite,
                           assert_allclose)


import shutil
from tempfile import mkdtemp
import gc

def test_1_storage(tmpdir):
    """
    Test functionality of base Storage object
    """
#-------------------------------------------------------------------------
# an example of using the Storage class
    import numpy as np
# example loading a variable and saving
    Storage.root = tmpdir.dirname
    Storage._nameargs.append('NameArgument')
    s = Storage('s', NameArgument='hello', family='myFAMILY')
    s = Storage(s)
    s.data['somedata'] = np.ones(5)
    s.variables.store(name=3)

    s.save(write_mode='hickle')
    s.save(write_mode='hickle')
    

    child = Storage('child', family='myFAMILY')
    child.parents = s
    assert_((len(s.children) == 1))
    assert_((child in s.children))
    child.data['childs data'] = 'anything'
    
    IM.common.cfg['WRITE_MODE'] = 'hickle'
    fname = child.save()
    assert os.path.splitext(fname)[1] == '.h5'

    s_str = str(child)
    assert os.path.isfile(s_str)
    rep_s = repr(s)

    del(s)
    del(child)
    import gc
    gc.collect()
    assert_(rep_s not in Storage._instances)

# test loading from a file
    s = Storage('s', NameArgument='hello')
    assert_(not s.loaded,
            's should have been re-initialised so should not be loaded')
    assert_('NameArgument' in s.name_atts, '')

# using representations
    rep = repr(s)
    assert_(s is eval(rep, Storage._objects))

    del(s)
    gc.collect()

    s = eval(rep, Storage._objects)

# parents
    r = Storage('testing2')
    s.parents = r

    assert_(r in s.parents)
    assert_(s in r.children)

    assert_(r in s.parents)

    del(r, s)


def test_children(tmpdir):
    a = Storage('a')
    b = Storage('b')
    b.parents = a

    a_new = Storage('a', force_new=True)

    assert_(not any(a_new.children),
            'Children should not belong to forced reloaded object')
