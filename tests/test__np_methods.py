
import ReadIM
import numpy as np
import gc
from tempfile import mkdtemp
import shutil
from numpy.testing import TestCase, assert_almost_equal, assert_equal, assert_, assert_raises, run_module_suite, assert_allclose
import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname( __file__), '..')))
import IM



def test_compare(tmpdir):
    # comparison tests

    v = IM.demo_VC7()
    vv = v.copy()
    v += 2

    # these assertions test all elements in the array of the object not
    # the result
    assert_((v == vv + 2).all(), 'introspective equality test failed')
    assert_((v != vv).all(), 'introspective inequality test failed')

    for a in v:
        print(a)

    factor = 2.5
    assert factor != 0
    for ax in v.axes:
        ax[:] = factor

    v2 = v
    assert_(v is v2)
    assert_((v == factor).all(), 'equality test failed')
    assert_((v <= factor).all(), 'equality test failed')
    assert_((v >= factor).all(), 'equality test failed')

    assert_((v != factor - 1).all(), 'inequality test failed')
    assert_((v > factor - 1).all(), 'inequality test failed')
    assert_((v <= factor + 1).all(), 'inequality test failed')

def test__lt__(tmpdir):
    for obj in [IM.demo_VC7(), IM.demo_IM7(), IM.demo_VC7(
            dtype='complex'), IM.demo_IM7(dtype='complex')]:
        obj[:] = 2.3
        assert_((obj < 3).all(), 'failed equality test')
        obj[0, 0, 0] = 4
        assert_(not (obj < 3).all(), 'single element failed')
        assert_((obj < 3).any(), 'failed equality test')
        obj[:] = 4
        assert_(not (obj < 3).any(), 'failed global equality test')

def test__le__(tmpdir):
    for obj in [IM.demo_VC7(), IM.demo_IM7()]:
        obj[:] = 2.3
        assert_((obj <= 2.3).all(), 'failed equality test')
        obj[0, 0, 0] = 3
        assert_(not (obj <= 2.3).all(), 'single element failed')
        assert_((obj <= 3).any(), 'failed equality test')
        obj[:] = 3
        assert_(not (obj <= 2.3).any(),
                'failed inverse global equality test')

def test__eq__(tmpdir):

    for obj in [IM.demo_VC7(), IM.demo_IM7()]:
        obj[:] = 1
        assert_((obj == 1).all(), 'all elements failed')
        obj[0, 0, 0] = 0
        assert_(not (obj == 1).all(), 'single element failed')
        assert_((obj == 1).any(), 'failed equality test')
        obj[:] = 0
        assert_(not (obj == 1).any(),
                'failed inverse global equality test')

def test__arrayOperations__(tmpdir):

    for obj in [IM.demo_VC7(), IM.demo_IM7()]:
        # generate arrays for diffent outcomes
        arr = obj.sum(1).sum(2)
        new = obj + arr
        assert_(new.frames == obj.frames, 'Operation not as expected')
        new = obj + arr[0]
        assert_(new.frames == obj.frames, 'Operation not as expected')

def test__ne__(tmpdir):
    for obj in [IM.demo_VC7(), IM.demo_IM7()]:
        obj[:] = 4
        assert_((obj != 3).all(), 'failed equality test')
        obj[0, 0, 0] = 3
        assert_(not (obj != 3).all(), 'single element failed')
        assert_((obj != 3).any(), 'failed equality test')
        obj[:] = 3
        assert_(not (obj != 3).any(),
                'failed inverse global equality test')

def test__ge__(tmpdir):
    for obj in [IM.demo_VC7(), IM.demo_IM7()]:
        obj[:] = 2.3
        assert_((obj >= 2.3).all(), 'failed equality test')
        obj[0, 0, 0] = 2
        assert_(not (obj >= 2.3).all(), 'single element failed')
        assert_((obj >= 2.3).any(), 'failed equality test')
        obj[:] = 2
        assert_(not (obj >= 2.3).any(),
                'failed inverse global equality test')

def test__gt__(tmpdir):
    for obj in [IM.demo_VC7(), IM.demo_IM7()]:
        obj[:] = 3
        assert_((obj > 2.3).all(), 'failed equality test')
        obj[0, 0, 0] = 2
        assert_(not (obj > 2.3).all(), 'single element failed')
        assert_((obj > 2.3).any(), 'failed equality test')
        obj[:] = 2
        assert_(not (obj > 2.3).any(),
                'failed inverse global equality test')

def test_arithmetic(tmpdir):

    v = IM.demo_VC7()
    vv = v.copy()

    factor = 2.5
    assert factor != 0
    for ax in v.axes:
        ax[:] = factor

    y = (v + 1)
    assert_((y == factor + 1).all(), 'addition error')

    y = (v - 1)
    assert_((y == factor - 1).all(), 'subtraction error')

    y = v * 2
    assert_((y == factor * 2).all(), 'product error')

    y = v / 2
    assert_((y == factor / 2).all(), 'division error')

    elements = np.prod(v.shape)
    assert_equal(v.sum(None), [elements * factor]
                    * 2, 'problem with summation')

    y = np.mean(v)
    im = v.new_im7(frames=6)
    im2 = im[0:v.frames]
    assert_equal(im2.frames, v.frames)
    s = im + im

    # you can also do math like operations. But should be the same shape (does not check spatial correctness)
    # the following are permitted...
    # see the npMethods Class for currently permitted operations and
    # behaviour
    print(v.mean(None))

    print(v.max(None))  # you can work with axis as well
    v.max()  # this is a new object but with only one frame
    v * v
    im + im
    v + 2
    v.abs()

    # note that the functionality always returns an array with the first
    # axis as [frame]
    im.sum(1)
    assert_((im.sum(1).sum(2) == im.sum(2).sum(2)).all())

    # same for vectors
    assert_((v.sum(1).sum(2) == v.sum(2).sum(2)).all())

    assert_almost_equal((v**2).Vx[0], factor**2)
    assert_almost_equal((v**2).Vy[0], factor**2)

    assert_equal((v * v).Vx, factor**2)
    assert_equal((v * v).Vy, factor**2)

    vmin = factor - factor - 1
    vmax = factor * 2 + 1
    v.Vx[0] = vmin
    v.Vy[0] = vmin
    v.Vx[1] = vmax
    v.Vy[1] = vmax

    assert_equal(v.min(None), [vmin] * 2)
    assert_equal(v.max(None), [vmax] * 2)

def test_any(tmpdir):

    for obj in [IM.demo_VC7(), IM.demo_IM7()]:
        obj[0, 0, 0] = 1
        assert_(obj.any(), 'failed to assign values')
        obj[0, 0, 0] = 0
        assert_(not obj.any(), 'failed to assign values')

def test_all(tmpdir):

    for obj in [IM.demo_VC7(), IM.demo_IM7()]:
        obj[:] = 1
        assert_(obj.all(), 'failed to assign values')
        obj[0, 0, 0] = 0
        assert_(obj.all() == False, 'failed to assign values')

def test__abs__(tmpdir):

    for obj in [IM.demo_VC7(), IM.demo_IM7()]:
        obj[:] = -1
        assert_((obj == -1).all(), 'failed to assign values')
        v_abs = abs(obj)
        assert_((v_abs == 1).all(), 'failed abs')

def test__add__(tmpdir):
    for obj in [IM.demo_VC7(), IM.demo_IM7()]:
        obj[:] = 5
        assert_((obj + obj == 10).all(), 'Addition failed')
        assert_((obj + 5 == 10).all(), 'Addition failed')

def test__div__(tmpdir):
    for obj in [IM.demo_VC7(), IM.demo_IM7()]:
        obj[:] = 5
    assert_((obj / 2 == 2.5).all(), 'division failed')

def test__floordiv__(tmpdir):
    for obj in [IM.demo_VC7(), IM.demo_IM7()]:
        obj[:] = 5
        assert_((obj // 2 == 2).all(), 'floor division failed')

def test__lshift__(tmpdir):
    """<< bitwise leftshift"""
    im = IM.demo_IM7()
    im = IM.IM7('new', buff=im.buffer, dtype=np.uint16)
    im[:] = 1
    assert_((im << 1 == 2).all())

def test__mod__(tmpdir):
    im = IM.demo_IM7()
    im[:] = 3
    assert_((im % 2 == 1).all(), 'modulo failed')

def test__mul__(tmpdir):
    for obj in [IM.demo_VC7(), IM.demo_IM7()]:
        obj[:] = 5.2
        assert_((obj * 3 == 5.2 * 3).all(), 'mul failed')

def test__pow__(tmpdir):
    for obj in [IM.demo_VC7(), IM.demo_IM7()]:
        obj[:] = 33.3
        assert_((obj**5 == 33.3**5).all(), 'multiplication failed')

def test__rshift__(tmpdir):
    im = IM.demo_IM7()
    im = IM.IM7('new', buff=im.buffer, dtype=np.uint16)
    im[:] = 1
    assert_((im >> 1 == 0).all())

def test__sub__(tmpdir):
    for obj in [IM.demo_VC7(), IM.demo_IM7()]:
        obj[:] = 33.3
        assert_((obj**5 == 33.3**5).all(), 'multiplication failed')

def test__truediv__(tmpdir):
    for obj in [IM.demo_VC7(), IM.demo_IM7()]:
        obj[:] = 5
    assert_((obj / 2 == 2.5).all(), 'division failed')
    assert_((obj / obj == 1).all(), 'division failed')

def test_ilshift__(tmpdir):
    im = IM.demo_IM7()
    im = IM.IM7('new', buff=im.buffer, dtype=np.uint16)
    im[:] = 1
    im <<= 1
    assert_((im == 2).all())

def test_iconcat__(tmpdir):
    pass

def test__iadd__(tmpdir):
    for obj in [IM.demo_VC7(), IM.demo_IM7()]:
        obj[:] = 2
        obj += 2
        assert_((obj == 4).all(), 'iadd failed')
        obj += obj
        assert_((obj == 8).all(), 'iadd failed by object')

def test__isub__(tmpdir):
    for obj in [IM.demo_VC7(), IM.demo_IM7()]:
        obj.all()
        obj[:] = 4
        obj -= 1
        assert_((obj == 3).all(), 'iadd failed')
        obj -= obj
        assert_((obj == 0).all(), 'iadd failed')

def test__sl__(tmpdir):
    for obj in [IM.demo_VC7(), IM.demo_IM7()]:
        sl = obj[0]
        sl2 = obj[0]
        assert_(sl is not sl2, 'slice should always be a new object')
