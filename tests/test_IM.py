
import ReadIM
import glob
import gc
from tempfile import mkdtemp
import shutil
from numpy.testing import TestCase, assert_almost_equal, assert_equal
from numpy.testing import assert_, assert_raises, run_module_suite, assert_allclose

import numpy as np
import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname( __file__), '..')))
import IM

def get_vec(factor=None):
    """ Returns a demo vector
    """
    v = IM.demo_VC7()
    if factor is not None:
        for ax in v.axes:
            ax[:] = factor
    return v


vectype_2D = [ReadIM.core.BUFFER_FORMAT_VECTOR_2D,
              ReadIM.core.BUFFER_FORMAT_VECTOR_2D_EXTENDED,
              ReadIM.core.BUFFER_FORMAT_VECTOR_2D_EXTENDED_PEAK,
              ]


def get_bfs():
    """
    Iterate through buffer formats available
    """
    for bf in ReadIM.BUFFER_FORMATS:
        if bf > 0 and bf not in vectype_2D:
            "buffer format not yet supported {0}".format(
                ReadIM.BUFFER_FORMATS[bf])
            continue
        yield bf


def test__setitem__(tmpdir):
    v = get_vec()
    v[:] = -1
    v[:, 1:, 1] = -3


def test__getitem__(tmpdir):
    v = get_vec()
    v2 = v[:]
    assert_((v2 == v).all())
    v3 = v[0]
    assert_equal(v.shape[1:], v3.shape[1:])
    v4 = v[1:2]
    assert_equal(v.shape[1:], v4.shape[1:])
    v5 = v[[0, 1]]
    assert_equal(v.shape[1:], v5.shape[1:])


def testVC7_2C(tmpdir):

    # check files are available first
    
    filename_v = ReadIM.extra.get_sample_vector_filenames()[0]
    assert os.path.isfile(filename_v)

    v = IM.VC7(filename_v)
    assert not v.loaded
    assert v.attributes
    assert v.loaded
    out = v.writeVC7(tmpdir.dirname + '/test1')

    assert ((v - v) == 0).all()
    v.show()
    v.load(True)  # force reload

    v1 = IM.demo_VC7()

    outdir = v1.writeVC7(tmpdir.dirname + '/test2')

    v.show()
    reps = [repr(v) for v in [v, v1]]
    del(v)
    del(v1)

    gc.collect()
    for r in reps:
        assert_(r not in IM.IM7._instances,
                "failed to delete object: {0}".format(r))


def testVC7_3C(tmpdir):

    # check files are available first
    filename_v = ReadIM.extra.get_sample_vector_filenames()[0]
    assert os.path.isfile(filename_v)

    v = IM.VC7(filename_v)
    v.Vx

    # write vector to vc7
    newdir = v.writeVC7(tmpdir.dirname + "/test")
    filename = glob.glob(newdir + '/*.vc7')[0]

    v2 = IM.VC7(filename)
    assert max(abs(v-v2).max(None)) < 1e-3, 'Problem writing file'
    v - v


def testIM7_1_cam(tmpdir):
    filename_im = ReadIM.extra.get_sample_image_filenames()[0]
    assert os.path.isfile(filename_im)

    im = IM.IM7(filename_im)
    assert im.I.sum() == 81986285.0


def testIM7_2_cam(tmpdir):

    filename_im = ReadIM.extra.get_sample_image_filenames()[1]
    assert os.path.isfile(filename_im)

    im = IM.IM7(filename_im, dtype=np.uint16)
    assert im.I.dtype.name == 'uint16'
    assert im.I.sum() == 2500505349
    destfile = tmpdir.dirname + '/test.im7'
    im._im7_writer(destfile)
    assert (IM.IM7(destfile) == im).all()


def test_writeVC7(tmpdir):

    # demo file has two frames
    v = IM.demo_VC7()
    v._root = tmpdir.dirname

    # test write vector (splits to one frame per file.)
    folder = v.writeVC7(tmpdir.dirname + '/writevectors')
    import glob
    ptn = os.path.join(folder, '*.vc7')

    src = glob.glob(ptn)[0]
    vv = IM.VC7(src)
    assert vv is not v

    assert_almost_equal(v[0].Vx, vv.Vx)
    assert_almost_equal(v[0].Vy, vv.Vy)


def test_im7_writer(tmpdir):

    filename_im = ReadIM.extra.get_sample_image_filenames()[0]
    assert os.path.isfile(filename_im)

    im = IM.IM7(filename_im)

    # Test write image
    fname = tmpdir.dirname + '/test.im7'
    im._im7_writer(fname)
    im2 = IM.IM7(fname)
    assert (im2.I == im.I).all()




def test_mag(tmpdir):
    for obj in [IM.demo_VC7(), IM.demo_IM7()]:
        obj[:] = 2
        mag = (2**2 * len(obj.axes))**0.5
        omag = obj.mag().I
        assert_((omag.mask == False).all(), 'Masking issue')
        assert_((omag == mag).all(), 'Error calculating magnitude')


def test__nonzero__(tmpdir):

        # check files are available first
    filename_v = ReadIM.extra.get_sample_vector_filenames()[0]
    filename_im = ReadIM.extra.get_sample_image_filenames()[0]

    for obj in [IM.IM7(filename_im), IM.VC7(filename_v)]:
        obj[:] = 1
        assert_(obj, 'Object with data should be nonzero')
        obj[:] = 0
        assert_(
            not obj, 'Object without data should be nonzero. Except if loadfile')
        # test a non-zero will be true for a file
        obj[:] = 1
        fname = str(obj)
        cls = obj.__class__
        repr(obj)

        obj2 = cls(obj._loadfile, force_reload=True)

        assert_(obj2, 'an unloaded object with a loadfile should be nonzero')
        assert_(
            not obj2.loaded,
            'Previous test not valid if file is loaded')


def test_makemask(tmpdir):
    for bf in get_bfs():

        gc.collect()
        buff1 = ReadIM.newBuffer([[0, 10], [10, 0]], 10, 10,
                                 frames=2, image_sub_type=bf,
                                 vectorGrid=1)

        if bf > 0:
            cls = IM.VC7
        else:
            cls = IM.IM7

        v1 = cls('v1_{0}'.format(bf), buff=buff1)
        v1[:] = 1

        v1.mask_coordinates['b1'] = [
            [1, 1], [1, 2], [2, 2], [2, 1], [1, 1]]
        total = sum(v1.sum(None))
        mask = v1.make_mask('b1')
        assert_equal(mask.sum(), 96)


def test_newobj(tmpdir):

    v = IM.demo_VC7()
    im1 = v.new_im7()
    im2 = v.new_im7()
    assert_(im1 is not im2, 'new objects should be created')


def test_serialize(tmpdir):
    import io
    dst = io.BytesIO()
    a = IM.demo_VC7()
    a.save(dst, write_mode='pickle')
    b = IM.VC7(dst)
    assert (a.Vx == b.Vx).all()
    b.mask
    b.browse()