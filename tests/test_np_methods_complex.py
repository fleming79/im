import numpy as np
import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname( __file__), '..')))
import IM

def testImaginaryProduct(tmpdir):
    v = IM.demo_VC7(dtype='complex')
    v[:] = -1
    v[:, 1:, 1] = -3
    v2 = v * 2
    assert hasattr(v2.Vx, 'real')

    im = IM.demo_IM7(dtype='complex')
    im2 = im * 2
    assert hasattr(im2.I, 'real')


